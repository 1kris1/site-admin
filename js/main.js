jQuery(function($){
    $('.cd-testimonials-wrapper').flexslider({
        selector: ".cd-testimonials > li",
        animation: "slide",
        controlNav: false,
        slideshow: false,
        smoothHeight: true,
        start: function(){
            $('.cd-testimonials').children('li').css({
                'opacity': 1,
                'position': 'relative'
            });
        }
    });

    //open the testimonials modal page
    $('.cd-see-all').on('click', function(){
        $('.cd-testimonials-all').addClass('is-visible');
    });

    //close the testimonials modal page
    $('.cd-testimonials-all .close-btn').on('click', function(){
        $('.cd-testimonials-all').removeClass('is-visible');
    });
    $(document).keyup(function(event){
        //check if user has pressed 'Esc'
        if(event.which=='27'){
            $('.cd-testimonials-all').removeClass('is-visible');
        }
    });

    //build the grid for the testimonials modal page
    if($('.cd-testimonials-all-wrapper').length){
        $('.cd-testimonials-all-wrapper').children('ul').masonry({
            itemSelector: '.cd-testimonials-item'
        });
    }
    $('.left_menu ul li').on('click',function(){
        $('.left_menu ul li').removeClass('active_link');
        $(this).toggleClass('active_link');
    });
    $('.change_password').on('change',function(){
        if($(this).prop('checked')){
            $('.change_password_input').show()
        }
        else{
            $('.change_password_input').hide()
        }
    });
});